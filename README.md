SDK Core
=========

To use the library you 

1. Checkout and install in your local maven repository the following projects:

*  model-core ([https://bitbucket.org/cityenabler/model-core.git ](https://bitbucket.org/cityenabler/model-core.git ))
*  sdk-httpclient ([https://bitbucket.org/cityenabler/sdk-httpclient.git](https://bitbucket.org/cityenabler/sdk-httpclient.git))
*  sdk-databinder ([https://bitbucket.org/cityenabler/sdk-databinder.git](https://bitbucket.org/cityenabler/sdk-databinder.git))

2. for the default implementation of the sdk-httpclient and sdk-databinder please refer to:

*  sdk-defaultdatabinder ([https://bitbucket.org/cityenabler/sdk-defaultdatabinder.git](https://bitbucket.org/cityenabler/sdk-defaultdatabinder.git))
*  sdk-defaulthttpclient ([https://bitbucket.org/cityenabler/sdk-defaulthttpclient.git](https://bitbucket.org/cityenabler/sdk-defaulthttpclient.git))

3. Checkout this project with
> _git clone https://bitbucket.org/cityenabler/sdk-core.git_

4. Import the project in Eclipse ([http://www.eclipse.org/downloads/](http://www.example.com/))

5. execute _maven install_ on the project