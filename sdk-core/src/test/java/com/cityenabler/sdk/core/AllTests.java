package com.cityenabler.sdk.core;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.cityenabler.sdk.tests.CkanTest;
import com.cityenabler.sdk.tests.OrionTest;

@RunWith(Suite.class)
@SuiteClasses({OrionTest.class, CkanTest.class})
public class AllTests {

}
