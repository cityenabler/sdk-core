package com.cityenabler.sdk.tests;

import static org.junit.Assert.*;

import java.util.HashMap;
import javax.ws.rs.core.Response;

import org.junit.Test;

import com.cityenabler.sdk.core.tools.MashupEditor;
import com.cityenabler.sdk.httpclient.impl.HttpClientImpl;

public class DMETest {
	
	private static final String testHost = "cityenabler.eng.it";
	private static final Integer testPort = 80;
	private static final String testProtocol = "http";
	private static final Integer testMashupID = 127;
	
	private static Integer instanceref = -1;
	private static Integer clientref = -1;
	
	
	@Test
	public void dmeInstance() {
		MashupEditor o = MashupEditor.getInstance(testHost);
		o.setClient(new HttpClientImpl());
		
			instanceref = o.hashCode();
			clientref = o.getClient().hashCode();
		
		assertTrue(testHost.equals(o.getHost()));
		assertTrue(testPort.equals(o.getPort()));
		assertTrue(testProtocol.equals(o.getProtocol()));
	}
	
	@Test
	public void dmeRetrieveInstance() {
		MashupEditor o = MashupEditor.getInstance(testHost);
		
		assertTrue(instanceref.equals(o.hashCode()));
		assertTrue(clientref.equals(o.getClient().hashCode()));
		assertTrue(o.getClient() instanceof HttpClientImpl);
		
	}
	
	@Test
	public void dmeInvokeMahup() {
		MashupEditor o = MashupEditor.getInstance(testHost);
		Response r = o.invokeMashup(testMashupID, new HashMap<String, String>(), Response.class);
		assertTrue(new Integer(200).equals(r.getStatus()));
	}
	
	@Test
	public void dmeInvokeMahup2() {
		MashupEditor o = MashupEditor.getInstance(testHost);
		String r = o.invokeMashup(testMashupID, new HashMap<String, String>(), String.class);
		assertFalse(r.isEmpty());
	}
	
//	@Test
//	public void dmeInvokeMahupWithData() {
//		MashupEditor o = MashupEditor.getInstance(testHost);
//		Response r = o.invokeMashupWithData(testMashupID, false, new HashMap<String, String>(), null, Response.class);
//		assertTrue(new Integer(200).equals(r.getStatus()));
//	}
	
}
