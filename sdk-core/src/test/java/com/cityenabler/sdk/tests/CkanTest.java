package com.cityenabler.sdk.tests;

import static org.junit.Assert.*;

import javax.ws.rs.core.Response;

import org.junit.Test;

import com.cityenabler.sdk.core.tools.Ckan;
import com.cityenabler.sdk.httpclient.impl.HttpClientImpl;
import com.cityenabler.sdk.model.DatastoreResource;

public class CkanTest {
	
	private static final String testHost = "opendata.cedus.eu";
	private static final Integer testPort = 80;
	private static final String testProtocol = "http";
	private static final String testresource = "e45949f7-5c77-4830-aa0e-2a847985fe43";
	
	private static Integer instanceref = -1;
	private static Integer clientref = -1;
	
	
	@Test
	public void ckanInstance() {
		Ckan o = Ckan.getInstance(testHost);
		o.setClient(new HttpClientImpl());
		
			instanceref = o.hashCode();
			clientref = o.getClient().hashCode();
		
		assertTrue(testHost.equals(o.getHost()));
		assertTrue(testPort.equals(o.getPort()));
		assertTrue(testProtocol.equals(o.getProtocol()));
	}
	
	@Test
	public void ckanRetrieveInstance() {
		Ckan o = Ckan.getInstance(testHost);
		
		assertTrue(instanceref.equals(o.hashCode()));
		assertTrue(clientref.equals(o.getClient().hashCode()));
		assertTrue(o.getClient() instanceof HttpClientImpl);
		
	}
	
	@Test
	public void ckanGetResource() {
		Ckan c = Ckan.getInstance(testHost);
		Response r = c.getResource(testresource, Response.class);
		assertTrue(new Integer(200).equals(r.getStatus()));
	}
	
	@Test
	public void ckanGetCSVResource() {
		Ckan c = Ckan.getInstance(testHost);
		String r = c.getResource(testresource, String.class);
		assertNotNull(r);
		assertFalse(r.isEmpty());
	}
	@Test
	public void ckanGetDatastoreResource() {
		Ckan c = Ckan.getInstance(testHost);
		Response r = c.getDatastoreResource(testresource, Response.class);
		assertTrue(new Integer(200).equals(r.getStatus()));
	}
	@Test
	public void ckanGetDatastoreResource2() {
		Ckan c = Ckan.getInstance(testHost);
		DatastoreResource r = c.getDatastoreResource(testresource, DatastoreResource.class);
		assertNotNull(r);
		assertTrue(r.getSuccess());
		assertTrue(new Integer(1).equals(r.getResult().getTotal()));
	}
	
}
