package com.cityenabler.sdk.tests;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.TimeZone;

import javax.ws.rs.core.Response;

import org.junit.BeforeClass;
import org.junit.Test;

import com.cityenabler.sdk.core.tools.Orion;
import com.cityenabler.sdk.httpclient.impl.HttpClientImpl;
import com.cityenabler.sdk.model.NGSIAttribute;
import com.cityenabler.sdk.model.NGSISubscription;
import com.cityenabler.sdk.model.NGSISubscriptionCondition;
import com.cityenabler.sdk.model.NGSISubscriptionEntities;
import com.cityenabler.sdk.model.NGSISubscriptionHTTPField;
import com.cityenabler.sdk.model.NGSISubscriptionNotification;
import com.cityenabler.sdk.model.NGSISubscriptionSubject;
import com.cityenabler.sdk.model.databinding.DefaultDataBinder;
import com.cityenabler.sdk.model.fiwaredatamodel.WasteContainer;
import com.cityenabler.sdk.tool.SdkContext;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

@SuppressWarnings("deprecation")
public class OrionTest {
	
	private static final String testHost = "217.172.12.177";
	private static final Integer testPort = 1026;
	private static final String testProtocol = "http";
	private static final String testservice = "danilo";
	private static final String testservicepath = "/test";
	
	private static Integer instanceref = -1;
	private static Integer clientref = -1;
	
	
	private Date getInstant(){
		try{
			ISO8601DateFormat sdf = new ISO8601DateFormat();
			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			Date d = sdf.parse("2018-06-18T15:51:26Z");
			return d;
		}
		catch(Exception e){
			e.printStackTrace();
			return GregorianCalendar.getInstance().getTime();
		}
	}
	
	private WasteContainer getTestWasteContainer(){
		Date instant = getInstant();
		
		WasteContainer test = new WasteContainer("nino1");
		test.setAttribute("dateModified", instant);
		test.setAttribute("isleId", "idIsola");
		
		Optional<NGSIAttribute<String>> isleid = test.getIsleId();
		if(isleid.isPresent())
			isleid.get().addMetadata("TimeInstant", instant);
		
		return test;
	}
	
	@BeforeClass
	public static void init(){
		SdkContext.getIstance().setDataBinder(new DefaultDataBinder());
	}
	
	@Test
	public void orionInstance() {
		Orion o = Orion.getInstance(testHost);
		o.setClient(new HttpClientImpl());
		
			instanceref = o.hashCode();
			clientref = o.getClient().hashCode();
		
		assertTrue(testHost.equals(o.getHost()));
		assertTrue(testPort.equals(o.getPort()));
		assertTrue(testProtocol.equals(o.getProtocol()));
	}
	
	@Test
	public void orionRetrieveInstance() {
		Orion o = Orion.getInstance(testHost);
		
		assertTrue(instanceref.equals(o.hashCode()));
		assertTrue(clientref.equals(o.getClient().hashCode()));
		assertTrue(o.getClient() instanceof HttpClientImpl);
		
		Response r = o.getEntities(testservice, testservicepath, Response.class);
		assertTrue(new Integer(200).equals(r.getStatus()));
	}
	
	@Test
	public void orionCreateEntity() {
		Orion o = Orion.getInstance(testHost);
		WasteContainer wc = getTestWasteContainer();
		Response r = o.createEntity(testservice, testservicepath, wc, Response.class);
		assertTrue(new Integer(201).equals(r.getStatus()));
	}
	
	@Test
	public void orionGetAllEntities() {
		Orion o = Orion.getInstance(testHost);
		Response r = o.getEntities(testservice, testservicepath, Response.class);
		assertTrue(new Integer(200).equals(r.getStatus()));
	}
	
	@Test
	public void orionGetAllEntitiesWithParameters() {
		Orion o = Orion.getInstance(testHost);
		Map<String,String> params = new HashMap<String,String>();
			params.put("idPattern", "was");
			params.put("limit", "1");
		Response r = o.getEntities(testservice, testservicepath, params, Response.class);
		assertTrue(new Integer(200).equals(r.getStatus()));
	}
	
	@Test
	public void orionGetOneEntity() {
		Orion o = Orion.getInstance(testHost);
		WasteContainer r = o.getEntity(testservice, testservicepath, "nino1", WasteContainer.class);
		
		//Check the ID and type of the Entity
		assertTrue("nino1".equals(r.getId()));
		assertTrue("WasteContainer".equals(r.getType()));
		
		//Check the Optional Attribute isleId
		Optional<NGSIAttribute<String>> isleid = r.getIsleId();
		assertTrue(isleid.isPresent());
		assertTrue("idIsola".equals(isleid.get().getValue()));
		
		//Check the Mandatory Attribute dateModified
		assertTrue(getInstant().equals(r.getDateModified().getValue()));
	}
	
	@Test
	public void orionUpdateEntity() {
		Orion o = Orion.getInstance(testHost);
		WasteContainer wc = getTestWasteContainer();
		wc.setAttribute("isleId", "idIsolaNino");
		Response r = o.updateEntity(testservice, testservicepath, wc, Response.class);
		assertTrue(new Integer(204).equals(r.getStatus()));
		
	}
	
	@Test
	public void orionDeleteEntity() {
		Orion o = Orion.getInstance(testHost);
		Response r = o.deleteEntity(testservice, testservicepath, "nino1", Response.class);
		
		assertTrue(new Integer(204).equals(r.getStatus()));
	}
	
	@Test
	public void orionCreateSubscription() {
		Orion o = Orion.getInstance(testHost);
		String subId = "sub1";
		
		NGSISubscriptionEntities subscriptionEntity = new NGSISubscriptionEntities("5b44871635fb8eb717a6a073", false);
		NGSISubscriptionCondition subscriptionCondition = new NGSISubscriptionCondition(Arrays.asList("TimeInstant","timestamp"));
		NGSISubscriptionSubject subject = new NGSISubscriptionSubject(Arrays.asList(subscriptionEntity),subscriptionCondition);
		
		NGSISubscriptionNotification notification = new NGSISubscriptionNotification(11251, 
																					 "2018-09-25T08:07:22.00Z", 
																					 Arrays.asList(), 
																					 "normalized", 
																					 new NGSISubscriptionHTTPField("http://192.168.2.2:8085/dme/orion/device/manage/5"), 
																					 null, 
																					 "2018-09-25T08:07:23.00Z");
		
		NGSISubscription sb = new NGSISubscription(subId, subject, notification);
		Response r = o.createSubscription(testservice, testservicepath, sb, Response.class);
		assertTrue(new Integer(201).equals(r.getStatus()));
	}

	@Test
	public void orionGetAllSubscriptions() {
		Orion o = Orion.getInstance(testHost);
		Response r = o.getSubscriptions(testservice, testservicepath, Response.class);
		assertTrue(new Integer(200).equals(r.getStatus()));
	}
	
	@Test
	public void orionGetSubscription() {
		Orion o = Orion.getInstance(testHost);
		NGSISubscription r = o.getSubscription(testservice, testservicepath,"5bab939685536fe4610999d2");
		assertTrue("5bab939685536fe4610999d2".equals(r.getId()));
	}
	
	@Test
	public void orionDeleteSubscriptions() {
		Orion o = Orion.getInstance(testHost);
		Response r = o.deleteSubscription(testservice, testservicepath,"5bab939685536fe4610999d2", Response.class);
		assertTrue(new Integer(204).equals(r.getStatus()));
	}
	
	
}
