package com.cityenabler.sdk.core.tools;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MashupEditor extends Tool{

	private static final String DEFAULT_PROTOCOL = "http";
	private static final String DEFAULT_HOST = "localhost";
	private static final Integer DEFAULT_PORT = 80;
	
	private static final String invokationPath = "/dme/mashup/invoke-with-params/";
	
	public MashupEditor(String protocol, String host, Integer port){
		super(protocol, host, port);
	}
	public MashupEditor(String protocol, String host){
		this(protocol, host, DEFAULT_PORT);
	}
	public MashupEditor(String host, Integer port){
		this(DEFAULT_PROTOCOL, host, port);
	}
	public MashupEditor(String host){
		this(DEFAULT_PROTOCOL, host, DEFAULT_PORT);
	}
	public MashupEditor(){
		this(DEFAULT_PROTOCOL, DEFAULT_HOST, DEFAULT_PORT);
	}
	
	/**
	 * Create or retrieve an instance of MashupEditor object that allows to interact with the City Enabler </b>City Data Mashup Editor<b>.
	 * 
	 * @param protocol the protocol to use to connect with the Specific DME
	 * @param host the host where the specific DME instance is available
	 * @param port the port where the specific DME instance is available
	 * @return an instance of type MashupEditor.
	 */
	public static MashupEditor getInstance(String protocol, String host, Integer port) {
		return Tool.getInstance(MashupEditor.class, protocol, host, port);
	}
	
	
	/**
	 * Create or retrieve an instance of MashupEditor object that allows to interact with the City Enabler </b>City Data Mashup Editor<b>.
	 * The default port 80 will be used.
	 * 
	 * @param host the host where the specific DME instance is available
	 * @param port the port where the specific DME instance is available
	 * @return an instance of type MashupEditor.
	 */
	public static MashupEditor getInstance(String protocol, String host){
		return Tool.getInstance(MashupEditor.class, protocol, host, DEFAULT_PORT);
	}
	
	/**
	 * Create or retrieve an instance of MashupEditor object that allows to interact with the City Enabler </b>City Data Mashup Editor<b>.
	 * The default http protocol will be used.
	 * 
	 * @param host the host where the specific DME instance is available
	 * @param port the port where the specific DME instance is available
	 * @return an instance of type MashupEditor.
	 */
	public static MashupEditor getInstance(String host, Integer port){
		return Tool.getInstance(MashupEditor.class, DEFAULT_PROTOCOL, host, port);
	}
	
	/**
	 * Create or retrieve an instance of MashupEditor object that allows to interact with the City Enabler </b>City Data Mashup Editor<b>.
	 * The default http protocol and port 80 will be used.
	 * 
	 * @param host the host where the specific DME instance is available
	 * @return an instance of type MashupEditor.
	 */
	public static MashupEditor getInstance(String host){
		return Tool.getInstance(MashupEditor.class, DEFAULT_PROTOCOL, host, DEFAULT_PORT);
	}
	
	/**
	 * Perform a REST invocation of the mashup with ID <em>mashupid</em>, passing the <em>inputs</em> as query parameters.
	 *  
	 * @param mashupid the ID of the mashup to invoke
	 * @param inputs a map with all the inputs of the mashup.
	 * @param responseclass the expected ResponseClass
	 * @return the response of the mashup invocation as an object of type <em>responseclass</em>.
	 */
	public <T> T invokeMashup(Integer mashupid, Map<String, String> inputs, Class<T> responseclass){
		Map<String, List<String>> headers = new HashMap<String, List<String>>();

		String queryString = inputs.entrySet()
								.stream()
								.map(e -> e.getKey()+"="+e.getValue())
								.collect(Collectors.joining("&"));
		String url = getBaseurl()
						.concat(invokationPath)
						.concat(String.valueOf(mashupid))
						.concat("?").concat(queryString);
		
		return getClient().post(url, headers, null, responseclass);
	}
	
	/**
	 * Perform a REST invocation of the mashup with ID <em>mashupid</em>, passing the <em>inputs</em> as query parameters and <em>body</em> as payload of the request.
	 * This method can be used only to invoke mashup that has an input named "<em>data</em>".
	 * 
	 * @param mashupid the ID of the mashup to invoke
	 * @param hitTargets if true, all the notification toward the mashup's targets will be triggered.
	 * @param inputs a map with all the inputs of the mashup.
	 * @param body the payload of the request. This payload will be mapped to the mashup input field named 'data'
	 * @param responseclass the expected ResponseClass
	 * @return the response of the mashup invocation as an object of type <em>responseclass</em>.
	 */
	public <T, B> T invokeMashupWithData(Integer mashupid, boolean hitTargets, Map<String, String> inputs, B body, Class<T> responseclass){
		Map<String, List<String>> headers = new HashMap<String, List<String>>();

		Map<String, String> queryparams = new HashMap<String, String>(inputs);
		queryparams.put("hit_targets", String.valueOf(hitTargets));
		
		String queryString = queryparams.entrySet()
								.stream()
								.map(e -> e.getKey()+"="+e.getValue())
								.collect(Collectors.joining("&"));
		
		String url = getBaseurl()
						.concat(invokationPath)
						.concat(String.valueOf(mashupid))
						.concat("?").concat(queryString);
		
		return getClient().post(url, headers, body, responseclass);
	}
	
}
