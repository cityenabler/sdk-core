package com.cityenabler.sdk.core.tools;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Ckan extends Tool {

	private static final String DEFAULT_PROTOCOL = "http";
	private static final String DEFAULT_HOST = "localhost";
	private static final Integer DEFAULT_PORT = 80;
	private static final Integer DEFAULT_DATASTORE_LIMIT = 5;
	private static final Integer DEFAULT_DATASTORE_OFFSET = 0;
	
	private static final String resourcesbasepath = "/datastore/dump/";
	private static final String datastoresearchpath = "/api/3/action/datastore_search";
	
	public Ckan(String protocol, String host, Integer port){
		super(protocol, host, port);
	}
	public Ckan(String protocol, String host){
		this(protocol, host, DEFAULT_PORT);
	}
	public Ckan(String host, Integer port){
		this(DEFAULT_PROTOCOL, host, port);
	}
	public Ckan(String host){
		this(DEFAULT_PROTOCOL, host, DEFAULT_PORT);
	}
	public Ckan(){
		this(DEFAULT_PROTOCOL, DEFAULT_HOST, DEFAULT_PORT);
	}
	
	//Instance GETTER
	public static Ckan getInstance(String protocol, String host, Integer port) {
		return Tool.getInstance(Ckan.class, protocol, host, port);
	}
	public static Ckan getInstance(String protocol, String host){
		return Tool.getInstance(Ckan.class, protocol, host, DEFAULT_PORT);
	}
	public static Ckan getInstance(String host, Integer port){
		return Tool.getInstance(Ckan.class, DEFAULT_PROTOCOL, host, port);
	}
	public static Ckan getInstance(String host){
		return Tool.getInstance(Ckan.class, DEFAULT_PROTOCOL, host, DEFAULT_PORT);
	}
	
	
	public <T> T getResource(String resourceid, Class<T> responseclass){
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		return getClient().get(getBaseurl().concat(resourcesbasepath).concat(resourceid), headers, responseclass);
	}
	public <T> T getDatastoreResource(String resourceid, Class<T> responseclass){
		return getDatastoreResource(resourceid, DEFAULT_DATASTORE_OFFSET, DEFAULT_DATASTORE_LIMIT, responseclass);
		
	}
	public <T> T getDatastoreResource(String resourceid, Integer limit, Class<T> responseclass){
		return getDatastoreResource(resourceid, DEFAULT_DATASTORE_OFFSET, limit, responseclass);
	}
	public <T> T getDatastoreResource(String resourceid, Integer limit, Integer offset, Class<T> responseclass){
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		return getClient().get(getBaseurl().concat(datastoresearchpath)
									.concat("?resource_id=").concat(resourceid)
									.concat("&offset=".concat(String.valueOf(offset)))
									.concat("&limit=").concat(String.valueOf(limit)), 
									headers, responseclass);
	}
}
