package com.cityenabler.sdk.core.tools;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ws.rs.core.MediaType;

import com.cityenabler.sdk.model.NGSIEntity;
import com.cityenabler.sdk.model.NGSISubscription;
import com.cityenabler.sdk.serde.NgsiSerde;
import com.cityenabler.sdk.serde.SerdeDiscoveryService;

/**
 * 
 * @author SIRCHIA ANTONINO
 *
 */
public class Orion extends Tool {
	
	private static final String DEFAULT_PROTOCOL = "http";
	private static final String DEFAULT_HOST = "localhost";
	private static final Integer DEFAULT_PORT = 1026;
	
	private static final String entitiesbasepath = "/v2/entities/";
	private static final String subscriptionsbasepath = "/v2/subscriptions/";
	
	public Orion(String protocol, String host, Integer port){
		super(protocol, host, port);
	}
	public Orion(String protocol, String host){
		this(protocol, host, DEFAULT_PORT);
	}
	public Orion(String host, Integer port){
		this(DEFAULT_PROTOCOL, host, port);
	}
	public Orion(String host){
		this(DEFAULT_PROTOCOL, host, DEFAULT_PORT);
	}
	public Orion(){
		this(DEFAULT_PROTOCOL, DEFAULT_HOST, DEFAULT_PORT);
	}
	
	/**
	 * Create or retrieve an instance of Orion object that allows to interact with the City Enabler </b>Orion Context Broker<b>.
	 * 
	 * @param protocol the protocol to use to connect with the Specific Orion
	 * @param host the host where the specific Orion instance is available
	 * @param port the port where the specific Orion instance is available
	 * @return an instance of type Orion.
	 */
	public static Orion getInstance(String protocol, String host, Integer port) {
		return Tool.getInstance(Orion.class, protocol, host, port);
	}
	
	/**
	 * Create or retrieve an instance of Orion object that allows to interact with the City Enabler </b>Orion Context Broker<b>.
	 * The default port 1026 will be used.
	 * 
	 * @param protocol the protocol to use to connect with the Specific Orion
	 * @param host the host where the specific Orion instance is available
	 * @return an instance of type Orion.
	 */
	public static Orion getInstance(String protocol, String host){
		return Tool.getInstance(Orion.class, protocol, host, DEFAULT_PORT);
	}
	
	/**
	 * Create or retrieve an instance of Orion object that allows to interact with the City Enabler </b>Orion Context Broker<b>.
	 * The default http protocolwill be used.
	 * 
	 * @param host the host where the specific Orion instance is available
	 * @param port the port where the specific Orion instance is available
	 * @return an instance of type Orion.
	 */
	public static Orion getInstance(String host, Integer port){
		return Tool.getInstance(Orion.class, DEFAULT_PROTOCOL, host, port);
	}
	
	/**
	 * Create or retrieve an instance of Orion object that allows to interact with the City Enabler </b>Orion Context Broker<b>.
	 * The default http protocol and port 1026 will be used.
	 * 
	 * @param host the host where the specific Orion instance is available
	 * @return an instance of type Orion.
	 */
	public static Orion getInstance(String host){
		return Tool.getInstance(Orion.class, DEFAULT_PROTOCOL, host, DEFAULT_PORT);
	}
	
	
	public <T> T getEntities(String fiwareservice, String fiwareservicepath, Class<T> responseclass){
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		
		headers.put("fiware-service", Arrays.asList(fiwareservice));
		headers.put("fiware-servicepath", Arrays.asList(fiwareservicepath));
		
		return getClient().get(getBaseurl()+"/v2/entities", headers, responseclass);
	}
	
	public <T> T getEntities(String fiwareservice, String fiwareservicepath, Map<String,String> params, Class<T> responseclass){
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		
		headers.put("fiware-service", Arrays.asList(fiwareservice));
		headers.put("fiware-servicepath", Arrays.asList(fiwareservicepath));
		
		String queryString = params.entrySet().stream()
				.map(e -> {
					String v = e.getValue();
					try { v = URLEncoder.encode(e.getValue(), "UTF-8"); } 
					catch (Exception e1) { /* Do Nothing and leave value unchanged */ }
					
					return e.getKey().concat("=").concat(v);
				})
				.collect(Collectors.joining("&"));
		
		return getClient().get(getBaseurl()+"/v2/entities?".concat(queryString), headers, responseclass);
	}
	
	public <T extends NGSIEntity> T getEntity(String fiwareservice, String fiwareservicepath, String entityid, Class<T> responseclass){
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		
		headers.put("fiware-service", Arrays.asList(fiwareservice));
		headers.put("fiware-servicepath", Arrays.asList(fiwareservicepath));
		
		return getClient().get(getBaseurl().concat(entitiesbasepath).concat(entityid), headers, responseclass);
	}
	
	public <T, B extends NGSIEntity> T createEntity(String fiwareservice, String fiwareservicepath, B requestbody, Class<T> responseclass){
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		
		headers.put("fiware-service", Arrays.asList(fiwareservice));
		headers.put("fiware-servicepath", Arrays.asList(fiwareservicepath));
		List<String> content = new ArrayList<String>();
			content.add(MediaType.APPLICATION_JSON);
		headers.put("Content-Type", content);
		
		NgsiSerde<B> serde = SerdeDiscoveryService.findFor(requestbody);
		String stringifiedBody = serde.toJson(requestbody);
		
		return getClient().post(getBaseurl().concat(entitiesbasepath), headers, stringifiedBody, responseclass);
	}
	
	public <T> T deleteEntity(String fiwareservice, String fiwareservicepath, String entityid, Class<T> responseclass){
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		
		headers.put("fiware-service", Arrays.asList(fiwareservice));
		headers.put("fiware-servicepath", Arrays.asList(fiwareservicepath));
		
		return getClient().delete(getBaseurl().concat(entitiesbasepath).concat(entityid), headers, responseclass);
	}
	
	public <T, B extends NGSIEntity> T updateEntity(String fiwareservice, String fiwareservicepath, B entity, Class<T> responseclass) {
		
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		headers.put("fiware-service", Arrays.asList(fiwareservice));
		headers.put("fiware-servicepath", Arrays.asList(fiwareservicepath));
		List<String> content = new ArrayList<String>();
			content.add(MediaType.APPLICATION_JSON);
		headers.put("Content-Type", content);
		
		String url = getBaseurl().concat(entitiesbasepath).concat(entity.getId()).concat("/attrs");
			entity.setId(null);
			entity.setType(null);
		
		NgsiSerde<B> serde = SerdeDiscoveryService.findFor(entity);
		String stringifiedBody = serde.toJson(entity);
		return getClient().put(url, headers, stringifiedBody, responseclass);
	}
	
	//Subscriptions
	public <T> T getSubscriptions(String fiwareservice, String fiwareservicepath, Class<T> responseclass){
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		
		headers.put("fiware-service", Arrays.asList(fiwareservice));
		headers.put("fiware-servicepath", Arrays.asList(fiwareservicepath));
		
		return getClient().get(getBaseurl().concat(subscriptionsbasepath), headers, responseclass);
	}
	
	public NGSISubscription getSubscription(String fiwareservice, String fiwareservicepath, String subscriptionid){
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		
		headers.put("fiware-service", Arrays.asList(fiwareservice));
		headers.put("fiware-servicepath", Arrays.asList(fiwareservicepath));
		
		return getClient().get(getBaseurl().concat(subscriptionsbasepath).concat(subscriptionid), headers, NGSISubscription.class); 
	}
	
	public <T> T createSubscription(String fiwareservice, String fiwareservicepath, NGSISubscription requestbody, Class<T> responseclass){
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		
		headers.put("fiware-service", Arrays.asList(fiwareservice));
		headers.put("fiware-servicepath", Arrays.asList(fiwareservicepath));
		List<String> content = new ArrayList<String>();
			content.add(MediaType.APPLICATION_JSON);
		headers.put("Content-Type", content);
		
		NgsiSerde<NGSISubscription> serde = SerdeDiscoveryService.findFor(requestbody);
		String stringifiedBody = serde.toJson(requestbody);
		
		return getClient().post(getBaseurl().concat(subscriptionsbasepath), headers, stringifiedBody, responseclass);
	}
	
	public <T> T deleteSubscription(String fiwareservice, String fiwareservicepath, String subscriptionId, Class<T> responseclass){
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		
		headers.put("fiware-service", Arrays.asList(fiwareservice));
		headers.put("fiware-servicepath", Arrays.asList(fiwareservicepath));
		
		return getClient().delete(getBaseurl().concat(subscriptionsbasepath).concat(subscriptionId), headers, responseclass);
	}
	


}
