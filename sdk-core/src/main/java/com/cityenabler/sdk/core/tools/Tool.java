package com.cityenabler.sdk.core.tools;

import java.util.HashMap;
import java.util.Map;

import com.cityenabler.sdk.httpclient.HTTPClient;

@SuppressWarnings("unchecked")
public abstract class Tool{
	
	private static Map<String, Tool> instances;
	
	private String protocol;
	private String host;
	private Integer port;
	
	private String baseurl;
	private String key;
	
	private HTTPClient client;
	
	//Constructors
	public Tool(String protocol, String host, Integer port){
		this.setProtocol(protocol);
		this.setHost(host);
		this.setPort(port);
		
		this.setKey(buildKey(host, port));
		this.setBaseurl((protocol==null ? "http" : protocol).concat("://").concat(key));
	}
	
	public Tool(String host){
		this("http", host, 80);
	}
	public Tool(String host, Integer port){
		this("http", host, port);
	}
	public Tool(String protocol, String host){
		this(protocol, host, 80);
	}
	public Tool(){
		this("http", "localhost", 80);
	}
	
	
	//Getter and Setter
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public Integer getPort() {
		return port;
	}
	public void setPort(Integer port) {
		this.port = port;
	}

	public String getBaseurl() {
		return baseurl;
	}
	public void setBaseurl(String baseurl) {
		this.baseurl = baseurl;
	}
	protected void setKey(String key){
		this.key = key;
	}
	protected String getKey(){
		return this.key;
	}
	
	//Instance GETTER
	public static <T extends Tool> T getInstance(Class<T> toolClazz, String protocol, String host, Integer port) {
		if(toolClazz==null)
			throw new RuntimeException("Unable to create instance of tool "+toolClazz);
		
		if(host==null)
			throw new RuntimeException(toolClazz.getSimpleName()+" Host ["+host+"] not valid");
		
		if(instances == null){
			instances = new HashMap<String, Tool>();
		}
		
		String key = buildKey(host, port);
		if(instances.containsKey(key)){
			return (T)instances.get(key);
		}
		
		try{
			T instance = toolClazz.getConstructor(String.class, String.class, Integer.class).newInstance(protocol, host, port);
			instances.put(key, instance);
			return instance;
		}
		catch(Exception e){
			e.printStackTrace();
			return null;
		}
		
	}
	
	public static <T extends Tool> T getInstance(Class<T> toolClazz, String protocol, String host){
		return getInstance(toolClazz, protocol, host, null);
	}
	public static <T extends Tool> T getInstance(Class<T> toolClazz, String host, Integer port){
		return getInstance(toolClazz, null, host, port);
	}
	public static <T extends Tool> T getInstance(Class<T> toolClazz, String host){
		return getInstance(toolClazz, null, host, null);
	}
	
	private static String buildKey(String host, Integer port){
		return port==null || port==80 ? host : host.concat(":").concat(String.valueOf(port));
	}
	
	public void setClient(HTTPClient client){
		this.client = client;
	}
	public HTTPClient getClient(){
		return this.client;
	}
}
