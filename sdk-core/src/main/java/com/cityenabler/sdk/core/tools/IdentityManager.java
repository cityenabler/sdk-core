package com.cityenabler.sdk.core.tools;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ws.rs.core.MediaType;

public class IdentityManager extends Tool{

	private static final String DEFAULT_PROTOCOL = "http";
	private static final String DEFAULT_HOST = "localhost";
	private static final Integer DEFAULT_PORT = 80;
	
	private static final String getTokenPath = "/oauth2/token";
	private static final String validateTokenPath = "/user";
	
	
	public IdentityManager(String protocol, String host, Integer port){
		super(protocol, host, port);
	}
	public IdentityManager(String protocol, String host){
		this(protocol, host, DEFAULT_PORT);
	}
	public IdentityManager(String host, Integer port){
		this(DEFAULT_PROTOCOL, host, port);
	}
	public IdentityManager(String host){
		this(DEFAULT_PROTOCOL, host, DEFAULT_PORT);
	}
	public IdentityManager(){
		this(DEFAULT_PROTOCOL, DEFAULT_HOST, DEFAULT_PORT);
	}
	
	/**
	 * Create or retrieve an instance of IdentityManager object that allows to interact with the City Enabler </b>IdentityManager<b>.
	 * 
	 * @param protocol the protocol to use to connect with the Specific IDM
	 * @param host the host where the specific IDM instance is available
	 * @param port the port where the specific IDM instance is available
	 * @return an instance of type IdentityManager.
	 */
	public static IdentityManager getInstance(String protocol, String host, Integer port) {
		return Tool.getInstance(IdentityManager.class, protocol, host, port);
	}
	
	
	/**
	 * Create or retrieve an instance of IdentityManager object that allows to interact with the City Enabler </b>IdentityManager<b>.
	 * The default port 80 will be used.
	 * 
	 * @param host the host where the specific IDM instance is available
	 * @param port the port where the specific IDM instance is available
	 * @return an instance of type IdentityManager.
	 */
	public static IdentityManager getInstance(String protocol, String host){
		return Tool.getInstance(IdentityManager.class, protocol, host, DEFAULT_PORT);
	}
	
	/**
	 * Create or retrieve an instance of IdentityManager object that allows to interact with the City Enabler </b>IdentityManager<b>.
	 * The default http protocol will be used.
	 * 
	 * @param host the host where the specific IDM instance is available
	 * @param port the port where the specific IDM instance is available
	 * @return an instance of type IdentityManager.
	 */
	public static IdentityManager getInstance(String host, Integer port){
		return Tool.getInstance(IdentityManager.class, DEFAULT_PROTOCOL, host, port);
	}
	
	/**
	 * Create or retrieve an instance of IdentityManager object that allows to interact with the City Enabler </b>IdentityManager<b>.
	 * The default http protocol and port 80 will be used.
	 * 
	 * @param host the host where the specific IDM instance is available
	 * @return an instance of type IdentityManager.
	 */
	public static IdentityManager getInstance(String host){
		return Tool.getInstance(IdentityManager.class, DEFAULT_PROTOCOL, host, DEFAULT_PORT);
	}
	
	private <T> T getToken(String clientID, String clientSecret, Map<String, String> params, Class<T> responseclass){

		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		
		List<String> content = new ArrayList<String>();
			content.add(MediaType.APPLICATION_FORM_URLENCODED);
		headers.put("Content-Type", content);
		
		String basicAuthToken = Base64.getEncoder().encode(clientID.concat(":").concat(clientSecret).getBytes()).toString();
		
		headers.put("Authorization", Arrays.asList("Basic".concat(basicAuthToken)));
		
		String queryString = params.entrySet().stream()
											.map(e -> {
												String v = e.getValue();
												try { v = URLEncoder.encode(e.getValue(), "UTF-8"); } 
												catch (Exception e1) { /* Do Nothing and leave value unchanged */ }
												
												return e.getKey().concat("=").concat(v);
											})
											.collect(Collectors.joining("&"));
		
		String url = getBaseurl().concat(getTokenPath);
		
		return getClient().post(url, headers, queryString, responseclass);
	}
	
	/**
	 * This method allows to retrieve the token for a specific user at the end of the <em>Authorization Code Grant</em> flow.
	 * 
	 * @param clientID the client id of the Application registered in the IDM
	 * @param clientSecret the client secret of the Application registered in the IDM
	 * @param authorization_code the authozation_code provided after the login
	 * @param callbackurl the callback_url configured on the Application in IDM
	 * @param responseclass the expected ResponseClass
	 * @return the response of the IDM with the Authentication token, as an object of the responseclass
	 */
	public <T> T getTokenFromAuthorizationCode(String clientID, String clientSecret, String authorization_code, String callbackurl, Class<T> responseclass){
		Map<String, String> params = new HashMap<String, String>();
			params.put("grant_type", "authorization_code");
			params.put("code", authorization_code);
			params.put("redirect_uri", callbackurl);
			
		return getToken(clientID, clientSecret, params, responseclass);
	}
	
	/**
	 * This method allows to retrieve the token for a specific user at the end of the <em>Resource Owner Password Credentials Grant</em> flow.
	 * 
	 * @param clientID the client id of the Application registered in the IDM
	 * @param clientSecret the client secret of the Application registered in the IDM
	 * @param username the username of the user you want to login with
	 * @param password the password of the user you want to login with
	 * @param responseclass the expected ResponseClass
	 * @return the response of the IDM with the Authentication token, as an object of the responseclass
	 */
	public <T> T getTokenFromCredentials(String clientID, String clientSecret, String username, String password, Class<T> responseclass){
		Map<String, String> params = new HashMap<String, String>();
		params.put("grant_type", "password");
		params.put("username", username);
		params.put("password", password);
		
		return getToken(clientID, clientSecret, params, responseclass);
	}
	
	/**
	 * This method allows to retrieve the token for a specific user at the end of the <em>Client Credentials Grant</em> flow.
	 * 
	 * @param clientID the client id of the Application registered in the IDM
	 * @param clientSecret the client secret of the Application registered in the IDM
	 * @param responseclass the expected ResponseClass
	 * @return the response of the IDM with the Authentication token, as an object of the responseclass
	 */
	public <T> T getTokenFromClientAppCredentials(String clientID, String clientSecret, Class<T> responseclass){
		Map<String, String> params = new HashMap<String, String>();
		params.put("grant_type", "client_credentials");
		
		return getToken(clientID, clientSecret, params, responseclass);
	}
	
	/**
	 * This method allows to validate a token against the City Enabler Identity Manager
	 * 
	 * @param token the token to validate
	 * @param responseclass the expected ResponseClass
	 * @return the response of the IDM with the Authentication token, as an object of the responseclass
	 */
	public <T> T validateToken(String token, Class<T> responseclass){
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		String url = getBaseurl().concat(validateTokenPath);
		return getClient().head(url.concat("?access_token=").concat(token), headers, responseclass);
	}
	
	/**
	 * This method allows to obtain a fresh token for the specified application.
	 * 
	 * @param clientid the client id of the Application registered in the IDM
	 * @param clientsecret the client secret of the Application registered in the IDM
	 * @param refreshtoken the refreshtoken obtained with the token during the authentication flow
	 * @param responseclass the expected ResponseClass
	 * @return the response of the IDM with the Authentication token, as an object of the responseclass
	 */
	public <T> T refreshToken(String clientid, String clientsecret, String refreshtoken, Class<T> responseclass){
		Map<String, String> params = new HashMap<String, String>();
		params.put("grant_type", "refresh_token");
		params.put("client_id", clientid);
		params.put("client_secret", clientsecret);
		params.put("refresh_token", refreshtoken);
		
		return getToken(clientid, clientsecret, params, responseclass);
	}
	
}
